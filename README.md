Convert error log from `build_web_compilers:entrypont` on Angular7 projects.

_If you migrated your angulardart app to NNBD and face problem that on build
you got lots of errors saying that you passing nullable variables to
templates. This script may help you to look up where exactly this issue is._

From errors linked to template:
```
org-dartlang-app:///packages/tag_list_item.template.dart@8309+11:
Error: Property 'hasChildren' cannot be accessed on 'Tag?' because it is potentially null.
org-dartlang-app:///packages/audio_control.template.dart@5418+1:
Error: A value of type 'bool?' can't be assigned to a variable of type 'bool' because 'bool?' is nullable and 'bool' isn't.
org-dartlang-app:///packages/crop_form.template.dart@13623+6:
Error: Property 'height' cannot be accessed on 'CropModel?' because it is potentially null.
```

To errors linked to source file:
```
lib/src/tag_list_item.html:7:26: Error: Property 'hasChildren' cannot be accessed on 'Tag?' because it is potentially null.
lib/src/audio_control.html:8:26: Error: A value of type 'bool?' can't be assigned to a variable of type 'bool' because 'bool?' is nullable and 'bool' isn't.
lib/src/crop_form.html:5:81: Error: Property 'height' cannot be accessed on 'CropModel?' because it is potentially null.
```

# Usage
```
USAGE: web_compiler_extractor [-cvht]
-c,--compiler    Compiler (dart2js or ddc). [dart2js]
--template       Exclude template locations. [false]
--html           Exclude html locations. [false]
-v               Enable debug printing. [false]
-h               Print usage
EOF
```

**Example Usage**  
`cat <error-log> | web_compiler_extract [<compiler>]`

`compiler` can be one either `dart2js` or `ddc`. `dart2js` is default.

Build a project and pass those errors to this script.
```
pub run build_runner build web > build
cat build | web_compiler_extractor > issues
```

_You can ignore warnings - The script is trying to look harder for the source file._

Default output format is for vim users to fill out quickfix list.
```
vim -q <(cat issues)
```
To change format: Edit `V` function inside script.
